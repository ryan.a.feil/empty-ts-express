import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './redux/store';
import Greeting from './redux/greeting';

class App extends React.Component<null, null> {
	render() {
		return <div>
			<h1>Get Your App Started!</h1>
			<Greeting />
		</div>;
	}
}

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>, 
	document.getElementById('root'),
);
