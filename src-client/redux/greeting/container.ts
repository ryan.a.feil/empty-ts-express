import { connect } from 'react-redux';
import Greeting, { GreetingState, GreetingProps } from '../../view/greeting';
import { actions } from './reducer';

const mapStateToProps = (state: any, ownProps: any): GreetingProps => {
	return {
		message: state.greeting.greetingMessage,
	};
};

const mapDispatchToProps = (dispatch: any) => {
	return {
		onMessageChange: (event: React.ChangeEvent<HTMLInputElement>): void => {
			dispatch(actions.changeMessage(event.target.value));
		},
	};
};

const greetingContainer = connect(mapStateToProps, mapDispatchToProps)(Greeting);
export default greetingContainer;
