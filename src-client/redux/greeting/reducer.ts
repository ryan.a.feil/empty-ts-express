import { PayloadAction } from '../util/types';
import { GreetingState } from '../../view/greeting';

export const types = {
	CHANGE_MESSAGE: 'CHANGE_MESSAGE',
};

export const actions = {
	changeMessage: (newMessage: string): PayloadAction<string> => {
		return {
			type: types.CHANGE_MESSAGE,
			payload: newMessage,
		};
	},
};

const initialState = {
	greetingMessage: 'Thanks for using the template',
};

const reducer = (state = initialState, action: PayloadAction<any> = { type: null, payload: null }) => {
	switch (action.type) {
		case types.CHANGE_MESSAGE: {
			return Object.assign({}, state, { greetingMessage: action.payload } as GreetingState);
		} default: {
			return state;
		}
	}
};

export default reducer;
