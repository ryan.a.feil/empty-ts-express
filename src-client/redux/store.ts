import { greetingReducer } from './greeting';
import { combineReducers, createStore } from 'redux';

const masterReducer = combineReducers({
	greeting: greetingReducer,
});

const store = createStore(masterReducer);
export default store;
