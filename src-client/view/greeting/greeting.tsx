import * as React from 'react';
import { GreetingProps } from './props';

export default class Greeting extends React.Component<GreetingProps, {}> {
	constructor(props: GreetingProps) {
		super(props);
	}

	render(): JSX.Element {
		const { message } = this.props;
		return <div>
			<h2>Greetings to you!</h2>
			<p>Now a message: {message}</p>
		</div>;
	}
}
