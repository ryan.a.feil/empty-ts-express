import Greeting from './greeting';

export default Greeting;
export { GreetingProps } from './props';
export { GreetingState } from './state';
