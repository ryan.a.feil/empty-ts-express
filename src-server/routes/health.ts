import * as os from 'os';
import { Request, Response, Router } from 'express';
import { AppRoute } from '../types/appRoute';

/**
 * A route class for identifying whether or not the application is currently processing requests
 * @export
 * @class Health
 * @implements {AppRoute}
 */
export default class Health implements AppRoute {
	
	/**
	 * The path at which the Health route is mounted
	 * @memberof Health
	 */
	path = '/health';

	/**
	 * Return a response with a JSON object meant to test if the application is processing requests
	 * @param {Request} req 
	 * @param {Response} res 
	 * @returns {express.Response} 
	 * @memberof Health
	 */
	get(req: Request, res: Response): Response {
		return res.json({
			host: os.hostname(),
			status: 'Up and running.',
		});
	}

	/**
	 * Returns the Router containing definitions for the actions associated with this Route
	 * @returns {express.Router} 
	 * @memberof Health
	 */
	router(): Router {
		return Router().get('/', this.get);
	}
}
