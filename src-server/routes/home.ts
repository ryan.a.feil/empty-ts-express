import { Request, Response, Router } from 'express';
import { AppRoute } from '../types/appRoute';

/**
 * A route class for returning a character sheet page
 * @export
 * @class Health
 * @implements {AppRoute}
 */
export default class Home implements AppRoute {
	
	/**
	 * The path at which the Home route will be mounted
	 * @memberof Home
	 */
	path = '/';

	/**
	 * Return a response containing the HTML for the homepage
	 * @param {Request} req 
	 * @param {Response} res 
	 * @returns {Response}
	 * @memberof Home
	 */
	get(req: Request, res: Response): Response {
		return res.send(`
			<!DOCTYPE html>
			<html>
				<head>
					<meta charset="UTF-8">
					<title>TypeScript Express React Redux Template</title>
				</head>
				<body>
					<div id="root"></div>
					<script src="bundle-client.js"></script>
				</body>
			</html>
		`);
	}

	/**
	 * Returns the Router containing definitions for the actions associated with this Route
	 * @returns {Router} 
	 * @memberof Home
	 */
	router(): Router {
		return Router().get('/', this.get);
	}
}
