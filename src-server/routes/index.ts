import Health from './health';
import Home from './home';
import { AppRoute } from '../types/appRoute';

const routes: AppRoute[] = [
	new Health(),
	new Home(),
];

export default routes; 
