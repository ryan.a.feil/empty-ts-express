import * as express from 'express';
import * as http from 'http';
import routes from './routes';

/**
 * An instance of a NodeJS server configured to run an Express application in a cloud environment
 * @export
 * @class Server
 */
export default class Server {
	private application: express.Application;
	private httpServer: http.Server;

	constructor() {
		this.application = express();
		this.registerMiddleware();
		this.registerStaticContent();
		this.httpServer = this.createHttpServer();
	}

	/**
	 * Starts the http and https servers and the application configured to run on them.
	 * @memberof Server
	 */
	start(): void {
		this.startHttpServer();
		this.handleTerminationSignal();
	}

	/**
	 * Stops the http and https servers, shutting down the server.
	 * @memberof Server
	 */
	stop(): void {
		this.stopHttpServer();
	}

	/**
	 * Returns the http server's port number. Should return 8080 if port isn't assigned by environment.
	 * @readonly
	 * @private
	 * @type {number}
	 * @memberof Server
	 */
	private get httpPort(): number {
		return Number(process.env.PORT) || 8080;
	}

	/**
	 * Creates and returns a new NodeJS http server based on this server's Express application.
	 * @private
	 * @returns {http.Server} 
	 * @memberof Server
	 */
	private createHttpServer(): http.Server {
		return http.createServer(this.application);
	}

	/**
	 * Starts the NodeJS http server and sets it to listen on the configured port.
	 * @private
	 * @memberof Server
	 */
	private startHttpServer(): void {
		this.httpServer.listen(this.httpPort, () => {
			console.log(`The http server has started and is listening on port ${this.httpPort}.`);
		});
	}

	/**
	 * Stops the NodeJS http server and causes the its process to exit with code 0.
	 * @private
	 * @memberof Server
	 */
	private stopHttpServer(): void {
		this.httpServer.close(() => {
			console.log(`The http server listening on port ${this.httpPort} is shutting down.`);
			process.exit(0);
		});
	}

	/**
	 * Sets up an interrupt handler for the POSIX termination signal 'SIGTERM'. On the signal,
	 * the http will shut down and exit with code 0.
	 * @private
	 * @memberof Server
	 */
	private handleTerminationSignal(): void {
		process.on('SIGTERM', () => {
			console.log('A termination signal has been received by the server.');
			this.stop();
		});
	}

	/**
	 * Registers folders from which static content can be served
	 * @private
	 * @memberof Server
	 */
	private registerStaticContent(): void {
		this.application.use(express.static('./dist/public'));
	}

	/**
	 * Registers all routes and middleware with the application
	 * @private
	 * @memberof Server
	 */
	private registerMiddleware(): void {
		routes.forEach(route => this.application.use(route.path, route.router()));
	}
}
