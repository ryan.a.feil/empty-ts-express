import * as express from 'express';

/**
 * A class or object which represents a route at which the application will respond to requests
 * @export
 * @interface AppRoute
 */
export interface AppRoute {
	/**
	 * The path at which the Application route is to be mounted
	 * @type {string}
	 * @memberof AppRoute
	 */
	path: string;

	/**
	 * Returns the Router containing definitions for the actions associated with this Route
	 * @returns {express.Router} 
	 * @memberof AppRoute
	 */
	router(): express.Router;
}
