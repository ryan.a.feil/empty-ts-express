const clientConfig = {
	entry: "./src-client/app.tsx",
	target: "web",
	mode: "development",
	output: {
		filename: "bundle-client.js",
		path: __dirname + "/dist/public"
	},
	devtool: "source-map",
	resolve: {
		extensions: [".ts", ".tsx", ".js", ".json"]
	},
	module: {
		rules: [
			{ test: /\.tsx?$/, loader: "awesome-typescript-loader" },
			{ enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
		]
	}
}

const serverConfig = {
	entry: "./src-server/startServer.ts",
	target: "node",
	mode: "development",
	output: {
		filename: "bundle-server.js",
		path: __dirname + "/dist"
	},
	devtool: "source-map",
	resolve: {
		extensions: [".ts", ".js", ".json"]
	},
	module: {
		rules: [
			{ test: /\.tsx?$/, loader: "awesome-typescript-loader" },
			{ enforce: "pre", test: /\.js$/, loader: "source-map-loader" }
		]
	}
}

module.exports = [serverConfig, clientConfig];
